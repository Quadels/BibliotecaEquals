package vista.prestamo;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.ControladorPrestamo;
import gureSwing.GureJComboBox;
import modelo.Libro;
import modelo.Prestamo;

import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.SwingConstants;

public class DevolverPrestamo extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField nombreSocio;
	private JComboBox listaLibros;
	private ControladorPrestamo controladorPrestamo;
	private JTextField idLibro;
	private JTextField idSocio;

	public ControladorPrestamo getControladorPrestamo() {
		return controladorPrestamo;
	}

	public void setControladorPrestamo(ControladorPrestamo controladorPrestamo) {
		this.controladorPrestamo = controladorPrestamo;
	}

	public DevolverPrestamo(JDialog parent, boolean modal) {
		super(parent, modal);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());

		comboBoxLiburuak = new GureJComboBox();
		comboBoxLiburuak.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				comboBoxLiburuakActionPerformed();
			}
		});

		comboBoxLiburuak.setEditable(true);
		comboBoxLiburuak.setBounds(28, 55, 264, 20);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
				JLabel lblUtzitakoLiburuak = new JLabel("Libros prestados");
				lblUtzitakoLiburuak.setBounds(10, 58, 129, 14);
				contentPanel.add(lblUtzitakoLiburuak);
				contentPanel.add(comboBoxLiburuak);
				
						JLabel lblSozioIzena = new JLabel("Nombre de Socio");
						lblSozioIzena.setBounds(10, 100, 104, 14);
						contentPanel.add(lblSozioIzena);
						
								nombreSocio = new JTextField();
								nombreSocio.setEnabled(false);
								nombreSocio.setEditable(false);
								nombreSocio.setBounds(124, 94, 183, 20);
								contentPanel.add(nombreSocio);
								nombreSocio.setColumns(10);
														
														JButton btnDevolverPrestamo = new JButton("DEVOLVER PRESTAMO");
														btnDevolverPrestamo.addActionListener(new ActionListener() {
															public void actionPerformed(ActionEvent arg0) {
																//controladorPrestamo.devolverLibro(idLibro.getText(),idSocio.getText());
															}
														});
														btnDevolverPrestamo.setActionCommand("OK");
														btnDevolverPrestamo.setBounds(208, 199, 198, 23);
														contentPanel.add(btnDevolverPrestamo);
														
														JComboBox listaLibros = new JComboBox();
														listaLibros.setBounds(124, 52, 183, 20);
														contentPanel.add(listaLibros);
														
														JLabel lblDevolucionDeLibros = new JLabel("DEVOLUCI\u00D3N  DE LIBROS");
														lblDevolucionDeLibros.setHorizontalAlignment(SwingConstants.CENTER);
														lblDevolucionDeLibros.setFont(new Font("Tahoma", Font.BOLD, 18));
														lblDevolucionDeLibros.setBounds(61, 11, 300, 14);
														contentPanel.add(lblDevolucionDeLibros);
														
														idLibro = new JTextField();
														idLibro.setEnabled(false);
														idLibro.setColumns(10);
														idLibro.setBounds(371, 55, 53, 20);
														contentPanel.add(idLibro);
														
														JLabel label = new JLabel("Id");
														label.setBounds(338, 58, 16, 14);
														contentPanel.add(label);
														
														JLabel label_1 = new JLabel("Id");
														label_1.setBounds(339, 100, 16, 14);
														contentPanel.add(label_1);
														
														idSocio = new JTextField();
														idSocio.setEnabled(false);
														idSocio.setColumns(10);
														idSocio.setBounds(371, 97, 53, 20);
														contentPanel.add(idSocio);
	}

	protected void comboBoxLiburuakActionPerformed() {
		int inde = comboBoxLiburuak.getSelectedIndex();
		if ( comboBoxLiburuak.getSelectedIndex() > -1){//this.isVisible() &&
			controladorPrestamo.rellenarFormularioDeDevolucion((int) comboBoxLiburuak.getSelectedValue());
		}else{
			this.clearTextFields();
			
		}
	}



	public void comboaBete(ArrayList<Libro> liburuak) {

		for (Libro liburua : liburuak) {
			comboBoxLiburuak.addItem(liburua.getId(), liburua.getTitulo());
		}
		comboBoxLiburuak.setSelectedIndex(-1);
	}

	public void clear() {
		comboBoxLiburuak.removeAllItems();
		this.clearTextFields();
	}
	
	private void clearTextFields() {
		textFieldEgunKopurua.setText("");
		nombreSocio.setText("");
		
	}

	public void rellenarFormulario(Prestamo prestamo) {
		this.nombreSocio.setText(prestamo.getSocio().getNombre() + " " + prestamo.getSocio().getApellido());
		Date gaur = new Date();
		Date prestamoData = prestamo.getFecha();
		long egunak = (gaur.getTime() - prestamoData.getTime()) / (1000 * 24 * 60 * 60);
		this.textFieldEgunKopurua.setText(String.valueOf(egunak));
		if (egunak > 10) {
			this.textFieldEgunKopurua.setDisabledTextColor(Color.RED);

		}else{
			this.textFieldEgunKopurua.setDisabledTextColor(Color.GRAY);
		}

	}
}
