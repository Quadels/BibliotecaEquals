package vista.prestamo;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.ControladorPrestamo;
import vista.*;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;

public class GestionPrestamo extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private ControladorPrestamo controladorPrestamo;
	private NuevoPrestamo nuevoPrestamo;

	public ControladorPrestamo getControladorPrestamo() {
		return controladorPrestamo;
	}

	public void setControladorPrestamo(ControladorPrestamo controladorPrestamo) {
		this.controladorPrestamo = controladorPrestamo;
	}

		
	public NuevoPrestamo getNuevoPrestamo() {
		return nuevoPrestamo;
	}

	public void setNuevoPrestamo(NuevoPrestamo nuevoPrestamo) {
		this.nuevoPrestamo = nuevoPrestamo;
	}

	/**
	 * Create the dialog.
	 */
	public GestionPrestamo(JFrame principal, boolean modal) {
		super(principal,true);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JButton devolver = new JButton("Devolver un Libro");
		devolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				//controladorPrestamo.abrirDevolverPrestamo();
				
			}
		});
		devolver.setBounds(113, 69, 183, 23);
		contentPanel.add(devolver);
		
		JButton coger = new JButton("Coger un Libro");
		coger.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				controladorPrestamo.abrirNuevoPrestamo();
			}
		});
		coger.setBounds(113, 103, 183, 23);
		contentPanel.add(coger);
		
		JButton listarNoDevueltos = new JButton("Listar Libros No Devueltos");
		listarNoDevueltos.setBounds(113, 137, 183, 23);
		contentPanel.add(listarNoDevueltos);
		
		JButton listarPorSocio = new JButton("Listar por Socio");
		listarPorSocio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				controladorPrestamo.abrirListarPorSocio();
				
			}
		});
		listarPorSocio.setBounds(113, 175, 183, 23);
		contentPanel.add(listarPorSocio);
		
		JLabel lblGestionDePrestamo = new JLabel("GESTION DE PRESTAMO");
		lblGestionDePrestamo.setBounds(10, 11, 414, 22);
		contentPanel.add(lblGestionDePrestamo);
		lblGestionDePrestamo.setHorizontalAlignment(SwingConstants.CENTER);
		lblGestionDePrestamo.setFont(new Font("Tahoma", Font.BOLD, 18));
		
		JButton listarPorLibro = new JButton("Listar por Libro");
		listarPorLibro.setBounds(113, 214, 183, 23);
		contentPanel.add(listarPorLibro);
	}
}
