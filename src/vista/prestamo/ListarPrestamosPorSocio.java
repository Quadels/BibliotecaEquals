package vista.prestamo;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

import controlador.ControladorPrestamo;
import modelo.Libro;
import modelo.Prestamo;
import modelo.Socio;

import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.awt.event.ItemEvent;
import java.awt.Font;
import javax.swing.SwingConstants;

public class ListarPrestamosPorSocio extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTable tabla;

	private ControladorPrestamo controladorPrestamo;
	private JComboBox listaSocios;

	public ListarPrestamosPorSocio(JDialog parent, boolean modal) {

		super(parent, true);

		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);
		contentPanel.setBounds(0, 0, 434, 261);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel);
		contentPanel.setLayout(null);

		listaSocios = new JComboBox();
		listaSocios.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {

				controladorPrestamo.verPrestamosSocio(String.valueOf(listaSocios.getSelectedItem()));
			}
		});
				
				JLabel lblListadoDePrestamos = new JLabel("LISTADO DE PRESTAMOS POR SOCIO");
				lblListadoDePrestamos.setHorizontalAlignment(SwingConstants.CENTER);
				lblListadoDePrestamos.setFont(new Font("Tahoma", Font.BOLD, 18));
				lblListadoDePrestamos.setBounds(44, 22, 355, 14);
				contentPanel.add(lblListadoDePrestamos);
				
				JScrollPane scrollPane = new JScrollPane();
				scrollPane.setBounds(31, 124, 393, 126);
				contentPanel.add(scrollPane);
		
				tabla = new JTable();
				scrollPane.setViewportView(tabla);
		listaSocios.setBounds(116, 55, 242, 20);
		contentPanel.add(listaSocios);

		JLabel lblSocios = new JLabel("Socios");
		lblSocios.setBounds(10, 58, 72, 14);
		contentPanel.add(lblSocios);
	}

	public ControladorPrestamo getControladorPrestamo() {
		return controladorPrestamo;
	}

	public void setControladorPrestamo(ControladorPrestamo controladorPrestamo) {
		this.controladorPrestamo = controladorPrestamo;
	}

	public void rellenarTabla(Socio socio) {

		DefaultTableModel dtm = new DefaultTableModel();

		tabla.removeAll();

		ArrayList<Prestamo> prestamosSocio;
		String[] encabezados = { "TITULO", "FECHA", "DEVUELTO SI/NO" };

		dtm.setColumnIdentifiers(encabezados);
		
		prestamosSocio = socio.getPrestamos();

		for (Prestamo prestamo : prestamosSocio) {

			String[] fila = { prestamo.getLibro().getTitulo(), String.valueOf(prestamo.getFecha()),
					String.valueOf(prestamo.isDevuelto()) };

			dtm.addRow(fila);
		}
		tabla.setModel(dtm);
		TableRowSorter<DefaultTableModel> modeloOrdenado = new TableRowSorter<DefaultTableModel>(dtm);
		tabla.setRowSorter(modeloOrdenado);
	}

	public void rellenarListaSocio(ArrayList<Socio> socios) {
		listaSocios.removeAllItems();
		
		for (Socio socio:socios){
			
			listaSocios.addItem(socio.getApellido()+", "+socio.getNombre());
			
		}
	}
}
