package vista.prestamo;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.ControladorPrestamo;
import modelo.Libro;
import modelo.Socio;
import vista.*;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class NuevoPrestamo extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private ControladorPrestamo controladorPrestamo;
	private JComboBox listaSocios;
	private JComboBox listaLibros;
	private JTextField idLibro;
	private JTextField idSocio;


	public ControladorPrestamo getControladorPrestamo() {
		return controladorPrestamo;
	}

	public void setControladorPrestamo(ControladorPrestamo controladorPrestamo) {
		this.controladorPrestamo = controladorPrestamo;
	}

	/**
	 * Create the dialog.
	 */
	public NuevoPrestamo(GestionPrestamo parent, boolean modal) {
		super(parent,true);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);
		contentPanel.setBounds(0, 0, 434, 261);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel);
		contentPanel.setLayout(null);
		
		listaLibros = new JComboBox();
		listaLibros.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				controladorPrestamo.seleccionarIdLibro(String.valueOf(listaLibros.getSelectedItem()));
			}
		});
		listaLibros.setEditable(true);
		listaLibros.setBounds(60, 67, 266, 20);
		contentPanel.add(listaLibros);
		
		JLabel lblLibros = new JLabel("Libros");
		lblLibros.setBounds(10, 67, 46, 14);
		contentPanel.add(lblLibros);
		
		JLabel lblSocios = new JLabel("Socios");
		lblSocios.setBounds(10, 122, 46, 14);
		contentPanel.add(lblSocios);
		
		listaSocios = new JComboBox();
		listaSocios.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				controladorPrestamo.seleccionarIdSocio(String.valueOf(listaSocios.getSelectedItem()));
			}
		});
		listaSocios.setEditable(true);
		listaSocios.setBounds(60, 122, 266, 20);
		contentPanel.add(listaSocios);
		{
			JButton buttonPrestamo = new JButton("TOMAR PRESTADO");
			buttonPrestamo.setBounds(226, 169, 198, 23);
			contentPanel.add(buttonPrestamo);
			buttonPrestamo.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					
					controladorPrestamo.insertarPrestamo(idLibro.getText(),idSocio.getText(),false);
				}
			});
			buttonPrestamo.setActionCommand("OK");
			getRootPane().setDefaultButton(buttonPrestamo);
		}
		
		JLabel lblPrestamodsDeLibros = new JLabel("PRESTAMOS DE LIBROS");
		lblPrestamodsDeLibros.setHorizontalAlignment(SwingConstants.CENTER);
		lblPrestamodsDeLibros.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblPrestamodsDeLibros.setBounds(67, 21, 300, 14);
		contentPanel.add(lblPrestamodsDeLibros);
		
		idLibro = new JTextField();
		idLibro.setEnabled(false);
		idLibro.setBounds(371, 67, 53, 20);
		contentPanel.add(idLibro);
		idLibro.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Id");
		lblNewLabel.setBounds(336, 70, 16, 14);
		contentPanel.add(lblNewLabel);
		
		JLabel label = new JLabel("Id");
		label.setBounds(336, 125, 16, 14);
		contentPanel.add(label);
		
		idSocio = new JTextField();
		idSocio.setEnabled(false);
		idSocio.setColumns(10);
		idSocio.setBounds(371, 122, 53, 20);
		contentPanel.add(idSocio);
	}


	public void rellenarTablaLibros(ArrayList<Libro> libros) {
		
		listaLibros.removeAllItems();
		
		for (Libro libro:libros){
			
			listaLibros.addItem(libro.getTitulo());
			//idSocio.setText(String.valueOf(libro.getId()));
		}
		
	}

	public void rellenarTablaSocios(ArrayList<Socio> socios) {
	listaSocios.removeAllItems();
		
		for (Socio socio:socios){
			
			listaSocios.addItem(socio.getApellido()+", "+socio.getNombre());
			
		}
		
	}

	public void rellenarIdLibro(int id) {
		
		idLibro.setText(String.valueOf(id));
		
	}
	public void rellenarIdSocio(int id) {
		
		idSocio.setText(String.valueOf(id));
		
	}
}
