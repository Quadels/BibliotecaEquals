package controlador;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.JOptionPane;

import modelo.Libro;
import modelo.ModeloLibro;
import modelo.ModeloPrestamo;
import modelo.ModeloSocio;
import modelo.Prestamo;
import modelo.Socio;
import vista.*;
import vista.prestamo.*;


public class ControladorPrestamo {

	private Principal principal;
	private GestionPrestamo gestionPrestamo;
	private NuevoPrestamo nuevoPrestamo;
	private ListarPrestamosPorSocio  listarPrestamosPorSocio;
	
	
	private ModeloSocio modeloSocio;
	private ModeloLibro modeloLibro;
	private ModeloPrestamo modeloPrestamo;
	
	
	
	public ListarPrestamosPorSocio getListarPrestamosPorSocio() {
		return listarPrestamosPorSocio;
	}

	public void setListarPrestamosPorSocio(ListarPrestamosPorSocio listarPrestamosPorSocio) {
		this.listarPrestamosPorSocio = listarPrestamosPorSocio;
	}

	public GestionPrestamo getGestorPrestamo() {
		return gestionPrestamo;
	}

	public void setGestorPrestamo(GestionPrestamo gestorPrestamo) {
		this.gestionPrestamo = gestorPrestamo;
	}

	
	public ModeloPrestamo getModeloPrestamo() {
		return modeloPrestamo;
	}

	public void setModeloPrestamo(ModeloPrestamo modeloPrestamo) {
		this.modeloPrestamo = modeloPrestamo;
	}

	public GestionPrestamo getGestionPrestamo() {
		return gestionPrestamo;
	}

	public void setGestionPrestamo(GestionPrestamo gestionPrestamo) {
		this.gestionPrestamo = gestionPrestamo;
	}

	public Principal getPrincipal() {
		return principal;
	}

	public void setPrincipal(Principal principal) {
		this.principal = principal;
	}

	
	public NuevoPrestamo getNuevoPrestamo() {
		return nuevoPrestamo;
	}

	public void setNuevoPrestamo(NuevoPrestamo nuevoPrestamo) {
		this.nuevoPrestamo = nuevoPrestamo;
	}

	public ControladorPrestamo() {
		super();
		
		
	}
	
	public void abrirGestionPrestamo() {
		this.gestionPrestamo.setVisible(true);
	}
	
	

	public ModeloSocio getModeloSocio() {
		return modeloSocio;
	}

	public void setModeloSocio(ModeloSocio modeloSocio) {
		this.modeloSocio = modeloSocio;
	}

	public ModeloLibro getModeloLibro() {
		return modeloLibro;
	}

	public void setModeloLibro(ModeloLibro modeloLibro) {
		this.modeloLibro = modeloLibro;
	}

	public void abrirNuevoPrestamo() {
		ArrayList<Libro> libros=new ArrayList<Libro>();
		ArrayList<Socio> socios=new ArrayList<Socio>();
		
		try {
			libros=modeloPrestamo.cogerLibro();
			nuevoPrestamo.rellenarTablaLibros(libros);
			
			socios=modeloSocio.seleccionarTodos();
			nuevoPrestamo.rellenarTablaSocios(socios);
			
			nuevoPrestamo.setVisible(true);
			
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "ERROR AL INSERTAR PRESTAMO");
		}
	}
	public void seleccionarIdLibro(String titulo) {
		
		int id;
		try {
			id = modeloLibro.seleccionarIdLibro(titulo);
			nuevoPrestamo.rellenarIdLibro(id);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "ERROR AL INSERTAR PRESTAMO");
		}
	
	}
	
	public void seleccionarIdSocio(String nombre) {
		
		int id;
		try {
			id = modeloSocio.seleccionarIdSocio(nombre);
			nuevoPrestamo.rellenarIdSocio(id);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "ERROR AL INSERTAR PRESTAMO");
		}
		
	}

	public void abrirListarPorSocio() {
		
		try {
			ArrayList<Socio> socios=modeloSocio.seleccionarTodos();
			listarPrestamosPorSocio.rellenarListaSocio(socios);
			listarPrestamosPorSocio.setVisible(true);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "ERROR AL SELECCIONAR SOCIOS");
		}

	}

	public void verPrestamosSocio(String nombre) {

		try {
			 Socio socio=modeloSocio.seleccionarPrestamosSocio(nombre);
			listarPrestamosPorSocio.rellenarTabla(socio);
			
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "ERROR AL LISTAR PRESTAMOS POR SOCIO");
		}
		
		
	}

	public void insertarPrestamo(String id_libro, String id_socio,boolean valor) {
		Prestamo prestamo=new Prestamo();
		
		prestamo.setId_libro(Integer.parseInt(id_libro));
		prestamo.setId_socio(Integer.parseInt(id_socio));
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date fecha = new Date();
		prestamo.setFecha(fecha);
		
		prestamo.setDevuelto(valor);
		try {
			modeloPrestamo.insertarPrestamo(prestamo);
			JOptionPane.showMessageDialog(null, "PRESTAMO INSERTADO CORRECTAMENTE");
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "ERROR AL INSERTAR PRESTAMO");
		}
		
	}

	


}
